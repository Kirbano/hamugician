oPlayer.state = PLAYERSTATE.TALK;

// Este 1 se puede cambiar por una variable global que controle la velocidad del texto.
var _messageLength = string_length(message);
if (textProgress <= _messageLength){
	if soundAlarm = 0 {
		audio_play_sound(soundVoice,1,0);
		soundAlarm = soundSpeed;
	} else soundAlarm -= 1;

}

textProgress += 0.5;
//Acción de la barra espaciadora.
if (keyboard_check_pressed(ord("T"))) {

	if (textProgress >= _messageLength) {
		audio_play_sound(sCursor01,1,false);
		instance_destroy();
	} else {
		if (textProgress > 2) {
			textProgress = _messageLength;
		}
	}
}