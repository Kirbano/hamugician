frame		= sPortraitFrame;
portrait	= sPortraits;

box_width		= 320;
box_height		= sprite_get_width(frame);
port_width		= sprite_get_width(portrait);
port_height		= sprite_get_height(portrait);
namebox_width	= 96;
namebox_height	= 20;

port_x		= (global.game_width - box_width - port_width) * 0.5;
port_y		= (global.game_height*0.98) - port_height;
box_x		= port_x + port_width;
box_y		= port_y;
box_x2		= box_x + box_width;
box_y2		= box_y + box_height;
namebox_x	= box_x;
namebox_y	= box_y - namebox_height;
namebox_x2	= namebox_x + namebox_width;
namebox_y2	= namebox_y + namebox_height;

x_buffer		= 10;
y_buffer		= 6;
text_x			= box_x + x_buffer;
text_y			= box_y + y_buffer;
name_text_x		= namebox_x + (namebox_width/2);
name_text_y		= namebox_y + (namebox_height/2);
text_max_width	= box_width - (2*x_buffer);

box_col = c_black;

portrait_index	= 0;
counter			= 0;
pause			= false;

cursor_index = 0;
cursor_paused = false;		//Controla la velocidad de la animación del cursor.


text[0] = ""
page	= 0;
name	= ""
voice	= sVoice002;

interact_key	= ord("E");
interact_button = gp_face2;

text_col		= c_white;
name_text_col	= c_white;
font			= fText;

draw_set_font(font);
text_height = string_height("M");

choice = 0;
choice_col = c_orange;