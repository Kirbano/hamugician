if (keyboard_check_pressed(interact_key) || gamepad_button_check_pressed(0,interact_button)) {
	if (counter < str_len and !choice_dialogue) { counter = str_len } else {
	audio_play_sound(sCursor01,1,false);
	if (page < array_length(text) - 1) {
		event_perform(ev_other, ev_user2);
		
		var line = next_line[page];
		if (choice_dialogue) line = line[choice];
		
		if (line == 0) page++;
		else if (line == -1) { instance_destroy(); exit; }
		else page = line;

		event_perform(ev_other, ev_user1);
	} else { instance_destroy(); }
	
	}
}

if (choice_dialogue) {
	choice += (keyboard_check_pressed(ord("S")) || gamepad_button_check_pressed(0, gp_padd)) - (keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(0, gp_padu))
	
	if (choice > text_array_len-1) choice = 0;
	if (choice < 0) choice = text_array_len-1;
	
	if (keyboard_check_pressed(ord("S")) || gamepad_button_check_pressed(0, gp_padd) || keyboard_check_pressed(ord("W")) || gamepad_button_check_pressed(0, gp_padu)) {
		audio_play_sound(sCursor02,11,false);
	}
}