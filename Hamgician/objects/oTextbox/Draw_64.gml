//Draw Box
// V En caso de querer usar un sprite para la textbox V
//draw_sprite(box, 0, box_x, box_y);
draw_set_colour(box_col);
draw_set_alpha(0.5);
draw_roundrect(port_x, box_y, box_x2, box_y2, 0);

draw_set_alpha(1);
draw_set_colour(c_white);
draw_roundrect(port_x, box_y, box_x2, box_y2, 1);

//Draw Portrait Back
//draw_sprite(frame, 0, port_x, port_y);

//Draw Portrait
draw_sprite(portrait, portrait_index, port_x, box_y2);


//Draw Portrait Frame
//draw_sprite(frame, 1, port_x, port_y);

//Draw Namebox
//Al igual que con DRAW BOX, aquí podemos usar un sprite cuando implementen el 9 slice.
draw_set_colour(box_col);
draw_set_alpha(0.5);
draw_roundrect(namebox_x, namebox_y, namebox_x2, namebox_y2, 0);
draw_set_alpha(1);
draw_set_colour(c_white);
draw_roundrect(namebox_x, namebox_y, namebox_x2, namebox_y2, 1);

draw_set_alpha(1);


//----TEXT
draw_set_font(font);

//Draw Name
var c = name_text_col;
draw_set_halign(fa_center); draw_set_valign(fa_middle);
draw_text_color(name_text_x, name_text_y, name, c, c, c, c, 1);
draw_set_halign(fa_left); draw_set_valign(fa_top);


if (!choice_dialogue) {
	//Draw Text
	if (!pause and counter < str_len) {
		counter++;
		if (counter mod 5 == 0) {
			audio_play_sound(voice, 10, false);
		}
	
		switch (string_char_at(text_wrapped, counter)) {
			case ",": pause = true; alarm[1] = 10; break;
			case ".":
			case "?":
			case "!": pause = true; alarm[1] = 15; break;
		}
	}
	var substr = string_copy(text_wrapped, 1, counter);


	c = text_col;
	draw_text_color(text_x, text_y, substr, c, c, c, c, 1);
	
	//Draw Cursor

	if (counter >= str_len) {
		draw_sprite(sCursor,cursor_index, box_x2-12, box_y2 - 6);
		if (cursor_paused = false) {
			cursor_index++;
			cursor_paused = true;
			alarm[2] = 5;
		}
	}

} else {
	c = text_col;
	var i = 0, y_add = 0; repeat(text_array_len){
		if (choice == i) { 
			c = choice_col
			
			draw_sprite(sCursor2,cursor_index, text_x-8, text_y+(text_height/2)+y_add);
			if (cursor_paused = false) {
				cursor_index++;
				cursor_paused = true;
				alarm[2] = 5;
			}
		}
		else c = text_col;
		
		draw_text_ext_color(text_x, text_y+y_add, text_array[i], text_height, text_max_width, c, c, c, c, 1);
		y_add += string_height_ext(text_array[i], text_height, text_max_width);
		i++
	}
}



