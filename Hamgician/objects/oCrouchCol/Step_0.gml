x = oPlayer.x + oPlayer.sprite_width/4;
y = oPlayer.y + oPlayer.sprite_height/4;

if (place_meeting(x, y, oWall)) {
	oPlayer.autoCrouch = false;
} else {
	if ( oPlayer.state = PLAYERSTATE.FREE and oPlayer.onTheGround ) {
		oPlayer.autoCrouch = true;
	} else { oPlayer.autoCrouch = false; }
}

if oPlayer.autoCrouch = true {
	image_index = 1;
} else { image_index = 0; }