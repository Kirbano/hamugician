var xCam = clamp(oPlayer.x - wCam / 2, 0, room_width - wCam);
var yCam = clamp(oPlayer.y - hCam / 2, 0, room_height - hCam);

var curX = camera_get_view_x(view_camera[0]);
var curY = camera_get_view_y(view_camera[0]);

var newX = lerp(curX, xCam, 0.1);
var newY = lerp(curY, yCam, 0.1);

camera_set_view_pos(view_camera[0], newX, newY);

layer_x(layer_get_id("Background"), newX * 0.1);
layer_y(layer_get_id("Background"), newY * 0.1);

layer_x(layer_get_id("Background2"), newX * 0.3);
layer_y(layer_get_id("Background2"), newY * 0.3);

layer_x(layer_get_id("Background3"), newX * 0.5);
layer_y(layer_get_id("Background3"), newY * 0.5);

layer_x(layer_get_id("Background4"), newX * 0.7);
layer_y(layer_get_id("Background4"), newY * 0.7);

layer_x(layer_get_id("Background5"), newX * 1);
layer_y(layer_get_id("Background5"), newY * 1);

layer_x(layer_get_id("Foreground"), newX * -0.1);
layer_y(layer_get_id("Foreground"), newY * -0.1);