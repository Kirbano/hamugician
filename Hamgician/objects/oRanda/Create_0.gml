can_move = true; // https://youtu.be/Srffc7MqWzs?t=1172


// Text-related variables
/*portrait_index = 1;
voice = sVoice001;
name = "Randa";*/

text = ["Oh, ¡hola, Hamuko! ¿Qué tal estás?",
			["¡Estoy bien!",
			"Lo cierto es que me he perdido.",
			"Chachi piruli lerendi."],
		"¡Genial! Por cierto, recuerda que puedes saltar pulsando B.",
			"Ah, sí, gracias.",
		"Prueba a treparte. Ya sabes que los hámsters sois muy buenos trepadores. ¡Puedes treparte por cualquier sitio!",
			"¡Qué bien lo sabes!",
		"Pero ¿tú de qué siglo eres?"];

speakers	= [id, oPlayer, id, oPlayer, id, oPlayer, id];
next_line	= [0, [2, 4, 6], 0, -1, 0, -1, -1];
scripts		=	[
					-1,
					[
						[change_variable, id, "voice", sVoice002],
						[change_variable, id, "voice", sVoice001],
						[change_variable, id, "portrait_index", 0]
					],
					-1, -1, -1, -1, -1
				];
//Esta mierda es un lío, fiu.
/* 
Para usar los scripts, tenemos que crear nuestro propio script, los que vienen en
GM por defecto no funcionan porque no tienen su id propia, pero los que tú creas sí.
Si pones el nombre del script sin (), GM toma su id.