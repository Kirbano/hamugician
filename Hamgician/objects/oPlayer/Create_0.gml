#region Basic Variables

spd = 0;
normal_spd = 4;
crouch_spd = 2;
xSpeed = 0;
ySpeed = 0;
grav = 0.5;
climb_grav = 0.125;
onTheGround = false;

autoCrouch = false;

state = PLAYERSTATE.FREE;

#endregion

#region Create Player Collision Controllers
instance_create_layer(x, y, "PlayerCol", oCrouchCol);
#endregion

#region Player States
enum PLAYERSTATE {
	FREE,
	TALK,
	CLIMB,
	CROUCH
}

#endregion

#region Text-related variables
portrait_index = 0;
voice = sVoice002;
name = "Hamuko";

radius = 32;
active_textbox = noone;
#endregion

#region Climbing Variables
climb_stamina_val = 32;
climb_stamina = climb_stamina_val;
fall_sound = true;
climbing_speed = 1;
#endregion