//Resize the application surface to be lower res (for pixel games + performance)
surface_resize(application_surface,640,360);
//application_surface_draw_enable(false);

//Shader uniform variable setup
u_pos = shader_get_uniform(shd_light, "u_pos");
u_pos2 = shader_get_uniform(shd_shadow, "u_pos");
u_zz = shader_get_uniform(shd_light, "zz");

//Vertex format and buffer setup
vertex_format_begin();
vertex_format_add_position_3d();
vf = vertex_format_end();
vb = vertex_create_buffer();

shad_surf = noone;