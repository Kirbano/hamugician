/// @function						jumpThroughCol
/// @param							object_name
function jumpThroughCol(){
	with (argument0) {
		if (other.ySpeed > 0) {
			if (place_meeting(x, y-ceil(other.ySpeed), other) and !place_meeting(x, y, other)) {
				other.y = ceil(other.y);
				while (!place_meeting(x, y-1, other)) {
					other.y += 1;
				}
				other.ySpeed = 0;
			}
		}
	}
}