// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerState_Talk(){
	var onTheGround = place_meeting(x, y + 1, oWall);
	ySpeed++;

	if (onTheGround) {
		sprite_index = sPlayerIdle_strip4;
	} else {
		sprite_index = sPlayerJump;
	}

	if (place_meeting(x + xSpeed, y, oWall)) {
	
		while (!place_meeting(x + sign(xSpeed), y, oWall)) {
			x += sign(xSpeed);
		}
		xSpeed = 0;
	}

	if (place_meeting(x, y + ySpeed, oWall)) {
	
		while (!place_meeting(x, y + sign(ySpeed), oWall)) {
			y += sign(ySpeed);
		}
	
		ySpeed = 0;
	}

	y += ySpeed;
}