// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function text_rando(){
text = ["Hello there. What's a ham-ham like you doing out here?",
		"Oh, I'm just out on an epic adventure!",
		"I see! That is very cool.\nWell I wish you a great adventure and a lovely day.",
		"Thank you, lovely stranger!"];

speakers = [id, oPlayer, id, oPlayer];
next_line = [0, 0, 0, -1];
scripts = -1;
}