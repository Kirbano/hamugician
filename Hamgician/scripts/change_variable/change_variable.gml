/// @function						change_variable
/// @param							id (instance id)
/// @param							var_name_string
/// @param							value
function change_variable(){
	with(argument0) {
		variable_instance_set(id, argument1, argument2);
	}
}

/* Como hace falta la id de una instancia y para facilitarnos un poco la vida,
en el primer argumento podemos usar el nombre del objeto si usamos un with statement
y en la función usamos la id. De esta forma, al poner 

change_variable(oPlayer, X, X);

en el script obtendremos -> variable_instance_set(oPlayer.id, X, X);

En el arg1, la variable que queremos cambiar tiene que ser una string por algún motivo.
No supone ningún problema, pero es algo que hay que tener en cuenta.