// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

/* call_text("message",voice,soundSpeed)
	
*/

function call_text(){
	if oPlayer.state = PLAYERSTATE.FREE {
	var _message = argument0;
	var _voice = argument1;
	var _soundSpeed = argument2;
	
	with (instance_create_layer(x,y,"Instances",oText)) {
		message = _message;
		soundVoice = _voice;
		soundSpeed = _soundSpeed;
	}
	}

}