// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerState_Crouch(){
	var xDirection = (keyboard_check(ord("D")) || gamepad_button_check(0, gp_padr)) - (keyboard_check(ord("A")) || gamepad_button_check(0, gp_padl));
	var yDirection = (keyboard_check(ord("S")) || gamepad_button_check(0, gp_padd)) - (keyboard_check(ord("W")) || gamepad_button_check(0, gp_padu));
	//var jump = keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, gp_face1)
	var onTheGround = place_meeting(x, y + 1, oWall);
	var input_interact = (keyboard_check_pressed(ord("E")) || (gamepad_button_check_pressed(0, gp_face2)));
	
	
	image_speed = 1;
	grav = 0.5;
	spd = crouch_spd;
	
	//Interactions with objects	
	
	#region TEXTBOX
	if (input_interact and onTheGround) {
		if (active_textbox == noone) {
			var inst = collision_rectangle(x-radius, y-radius, x+radius, y+radius, parNPC, false, false);
		
			if (inst != noone) {
				with (inst) {
					var tbox = create_textbox(text, speakers, next_line, scripts);
					can_move = false;
				}
				mask_index = sPlayerIdle_strip4;
				state = PLAYERSTATE.TALK;
				active_textbox = tbox;
			}
		}	
	}
	if (!instance_exists(active_textbox)) {
			active_textbox = noone;
	}
	#endregion
	
	//APPLY MOVEMENTS
	
	if (xDirection != 0) image_xscale = xDirection;

	xSpeed = xDirection * spd;
	ySpeed += grav;

	if (onTheGround) {
		fall_sound = true;
		climb_stamina = climb_stamina_val;
		if (xDirection != 0) { sprite_index = sPlayerCrouch; }
		else  { sprite_index = sPlayerCrouch;}
	}

if (place_meeting(x + xSpeed, y, oWall)) {
	
		while (!place_meeting(x + sign(xSpeed), y, oWall)) {
			x += sign(xSpeed);
		} 
		xSpeed = 0;
	} 

	x += xSpeed;

	if (place_meeting(x, y + ySpeed, oWall)) {
	
		while (!place_meeting(x, y + sign(ySpeed), oWall)) {
			y += sign(ySpeed);
		}
	
		ySpeed = 0;
	}

	y += ySpeed;
	
	// Exit crouch if button not held
	if yDirection != 1 and !place_meeting(x, y-sprite_height/2, oWall) {
		mask_index = sPlayerIdle_strip4; 
		state = PLAYERSTATE.FREE;
	}
	
}

