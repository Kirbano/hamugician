// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerState_Climb(){
	var xDirection = (keyboard_check(ord("D")) || gamepad_button_check(0, gp_padr)) - (keyboard_check(ord("A")) || gamepad_button_check(0, gp_padl));
	var onTheGround = place_meeting(x, y + 1, oWall);
	var stepSound = (keyboard_check_pressed(ord("D")) || gamepad_button_check_pressed(0, gp_padr)) - (keyboard_check_pressed(ord("A")) || gamepad_button_check_pressed(0, gp_padl));

	grav = climb_grav;
	
	//Interactions with objects
	
	
	//APPLY MOVEMENTS
	
	if (xDirection != 0) image_xscale = xDirection;

	xSpeed = xDirection * spd;
	ySpeed += grav;

	if (onTheGround) {
		fall_sound = true;
		state = PLAYERSTATE.FREE;
		climb_stamina = climb_stamina_val;
	} else {
		if (ySpeed >= 0) { sprite_index = sPlayerSlide } else sprite_index = sPlayerClimb;
	}

	if (place_meeting(x+xDirection, y, oWall)) {
		if (climb_stamina >= 0 ) {ySpeed = clamp(ySpeed, -1, -2);}
		}
	else {
		if (!place_meeting(x+image_xscale, y, oWall)) { state = PLAYERSTATE.FREE; }
	}
	
	if (place_meeting(x + xSpeed, y, oWall)) {
		/*while (!place_meeting(x + sign(xSpeed), y, oWall)) {
			x += sign(xSpeed);
		}*/
		xSpeed = 0;
	}

	x += xSpeed;

	if (place_meeting(x, y + ySpeed, oWall)) {
	
		while (!place_meeting(x, y + sign(ySpeed), oWall)) {
			y += sign(ySpeed);
		}
	
		ySpeed = 0;
	}
	
	y += ySpeed;
	
	// Usar xDirection aquí permite usar timing para subir más alto. Con ySpeed no se puede hacer esto.
	if (climb_stamina >= 0 and xDirection != 0 /*ySpeed <= 0*/) { climb_stamina--; }
	if (climb_stamina <= 0 and ySpeed >= 0 and fall_sound = true) {
		if (!audio_is_playing(sFall001)) {
			fall_sound = false;
			audio_play_sound(sFall001,9,false);
		}
	}
	// Change animation speed according to stamina left (the less you have the faster it plays)
	if (climb_stamina >= climb_stamina_val/4*3 ) { image_speed = climbing_speed; }
	else if (climb_stamina >= climb_stamina_val/4*2) { image_speed = climbing_speed*1.3; }
	else if (climb_stamina >= climb_stamina_val/4) { image_speed = climbing_speed*1.6; }
	
	//Pasito
	if stepSound = xDirection and stepSound != 0 and climb_stamina > 0 {
		audio_play_sound(sStep01,12,0);
	}
}